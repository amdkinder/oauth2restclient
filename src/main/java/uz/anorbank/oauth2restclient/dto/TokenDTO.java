package uz.anorbank.oauth2restclient.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;

@Data
public class TokenDTO {
    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("refresh_token")
    private String refreshToken;

    @JsonProperty("id_token")
    private String idToken;

    @JsonProperty("expires_in")
    private Integer expiresIn;

    @JsonProperty("refresh_expires_in")
    private Integer refreshExpiresIn;

    @JsonProperty("token_type")
    private String tokenType = "Bearer";

    @JsonProperty("scope")
    private String scope = "openid";

    @JsonProperty("session_state")
    private String sessionState;

    @JsonProperty("not_before_policy")
    private int notBeforePolicy;


    private Instant createdAt = Instant.now();
    private Instant refreshExpiredAt;

    private static int serverSeconds = 10;
    private Instant getExpiredAt() {
        return createdAt.plusSeconds(expiresIn - serverSeconds);
    }

    private Instant getRefreshExpiredAt() {
        return createdAt.plusSeconds(refreshExpiresIn - serverSeconds);
    }

    public boolean isExpire() {
        return getExpiredAt().isBefore(Instant.now());
    }

    public boolean isExpireRefresh() {
        return getRefreshExpiredAt().isBefore(Instant.now());
    }
}
