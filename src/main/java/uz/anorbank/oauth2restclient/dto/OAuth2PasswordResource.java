package uz.anorbank.oauth2restclient.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OAuth2PasswordResource extends OAuth2ClientResource {
    private String username;
    private String password;
}
