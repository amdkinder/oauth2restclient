package uz.anorbank.oauth2restclient.dto;

import lombok.Data;

@Data
public class OAuth2ClientResource {
    private String url;
    private String realm;
    private String clientId;
    private String clientSecret;
}
