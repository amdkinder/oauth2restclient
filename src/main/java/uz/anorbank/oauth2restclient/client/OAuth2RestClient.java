package uz.anorbank.oauth2restclient.client;

import org.springframework.http.HttpHeaders;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import uz.anorbank.oauth2restclient.dto.TokenDTO;

public class OAuth2RestClient extends RestTemplate {
    private final OAuth2UserClient oAuth2UserClient;

    public TokenDTO getToken() {
        return oAuth2UserClient.getTokenDTO();
    }

    public OAuth2RestClient(OAuth2UserClient oAuth2UserClient) {
        if (oAuth2UserClient == null) {
            throw new RuntimeException("Must be oauth2 rest client init with OAuth2UserClient");
        }
        var interceptors = this.getInterceptors();
        interceptors.add(oauth2Interceptor());
        this.oAuth2UserClient = oAuth2UserClient;
    }

    public ClientHttpRequestInterceptor oauth2Interceptor() {
        return (request, body, execution) -> {
            request.getHeaders().add(HttpHeaders.AUTHORIZATION, oAuth2UserClient.getAccessToken());
            return execution.execute(request, body);
        };
    }
}
