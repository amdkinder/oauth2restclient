package uz.anorbank.oauth2restclient.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import uz.anorbank.oauth2restclient.dto.OAuth2ClientResource;
import uz.anorbank.oauth2restclient.dto.TokenDTO;
import uz.anorbank.oauth2restclient.service.OAuth2ClientInterceptor;

import java.time.Duration;

@Slf4j
public abstract class OAuth2UserClient {

    private TokenDTO tokenDTO;

    public TokenDTO getTokenDTO() {
        getAccessToken();
        return tokenDTO;
    }

    public String getAccessToken() {
        if (tokenDTO != null) {
            if (!tokenDTO.isExpire())
                return tokenDTO.getAccessToken();
            else if (tokenDTO.isExpireRefresh()) {
                try {
                    tokenDTO = oAuth2ClientHelper().getRefreshToken(tokenDTO.getRefreshToken());
                } catch (Exception e) {
                    log.debug("Can not refresh token: {}", e.getMessage());
                    tokenDTO = oAuth2ClientHelper().getToken();
                }
            } else {
                tokenDTO = oAuth2ClientHelper().getToken();
            }
        } else {
            tokenDTO = oAuth2ClientHelper().getToken();
        }

        if (tokenDTO != null) {
            return tokenDTO.getAccessToken();
        } else return null;
    }

    public OAuth2ClientInterceptor oAuth2ClientHelper() {
        var restTemplate = new RestTemplateBuilder()
                .setReadTimeout(Duration.ofSeconds(3))
                .setConnectTimeout(Duration.ofSeconds(3))
                .build();
        return new OAuth2ClientInterceptor(restTemplate, getResource());
    }


    public abstract OAuth2ClientResource getResource();
}
