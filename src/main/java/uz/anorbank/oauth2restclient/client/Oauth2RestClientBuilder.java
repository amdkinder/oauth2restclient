package uz.anorbank.oauth2restclient.client;

import org.springframework.boot.web.client.RestTemplateBuilder;

public class Oauth2RestClientBuilder extends RestTemplateBuilder {

    private OAuth2UserClient oAuth2UserClient;

    public Oauth2RestClientBuilder setOauth2Client(OAuth2UserClient oAuth2UserClient) {
        this.oAuth2UserClient = oAuth2UserClient;
        return this;
    }

    public OAuth2RestClient build() {
        return configure(new OAuth2RestClient(oAuth2UserClient));
    }


}
