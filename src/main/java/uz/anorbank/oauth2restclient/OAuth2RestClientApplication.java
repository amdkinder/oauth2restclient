package uz.anorbank.oauth2restclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OAuth2RestClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(OAuth2RestClientApplication.class, args);
    }

}
