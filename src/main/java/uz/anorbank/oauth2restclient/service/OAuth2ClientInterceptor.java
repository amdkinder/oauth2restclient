package uz.anorbank.oauth2restclient.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import uz.anorbank.oauth2restclient.dto.OAuth2ClientResource;
import uz.anorbank.oauth2restclient.dto.OAuth2PasswordResource;
import uz.anorbank.oauth2restclient.dto.TokenDTO;
import uz.anorbank.oauth2restclient.exception.BadRequestException;

@Slf4j
@RequiredArgsConstructor
public class OAuth2ClientInterceptor {

    private final RestTemplate restTemplate;
    private final OAuth2ClientResource oAuth2Properties;


    public TokenDTO getToken() {
        return keycloakRequest(getTokenRequestMap());
    }

    public TokenDTO getRefreshToken(String refreshToken) {
        return keycloakRequest(getRefreshTokenRequestMap(refreshToken));
    }


    private String getTokenUrl() {
        return String.format("%s/realms/%s/protocol/openid-connect/token", oAuth2Properties.getUrl(), oAuth2Properties.getRealm());
    }

    private MultiValueMap<String, String> getTokenRequestMap() {
        var map = new LinkedMultiValueMap<String, String>() {
            {
                this.add("client_id", oAuth2Properties.getClientId());
                this.add("client_secret", oAuth2Properties.getClientSecret());
                this.add("grant_type", "client_credentials");
                this.add("scope", "openid");
            }
        };
        if (this.isPasswordResource()) {
            var passwordProperties = (OAuth2PasswordResource) oAuth2Properties;
            map.add("username", passwordProperties.getUsername());
            map.add("password", passwordProperties.getPassword());
            map.add("grant_type", "password");
        }
        return map;
    }

    private boolean isPasswordResource() {
        return oAuth2Properties instanceof OAuth2PasswordResource;
    }

    private MultiValueMap<String, String> getRefreshTokenRequestMap(String refreshToken) {
        return new LinkedMultiValueMap<>() {
            {
                this.add("client_id", oAuth2Properties.getClientId());
                this.add("client_secret", oAuth2Properties.getClientSecret());
                this.add("refresh_token", refreshToken);
                this.add("grant_type", "refresh_token");
                this.add("scope", "openid");
            }
        };
    }

    private TokenDTO keycloakRequest(MultiValueMap<String, String> map) {
        try {
            log.info("Request body: {}", map);
            ResponseEntity<TokenDTO> response = this.restTemplate.postForEntity(getTokenUrl(), map, TokenDTO.class);
            return response.getStatusCode() == HttpStatus.OK ? response.getBody() : null;
        } catch (Exception e) {
            log.warn("Can not get access token: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }


}
